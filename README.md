Users
=====

This role's duty is to set up users and groups.

Variables
---------

### `users_groups`
A list of groups to be created.

### `users`
A dictionary of users to be created, where:

- the key represents the username of the user to be created
- a **required** key `password` is the password to be set on user creation — it will not override current user password
- an *optional* key `group` is the primary group of the user
- an *optional* key `groups` is a list of additional groups
- an *optional* key `github` is the GitHub username associated to the user, to set its `authorized_keys` for SSH access

Example
-------

```yaml
---
users_groups:
  - sysadmins
  - othergroup
users:
  fquffio:
    password: $6$rounds=656000$6xPefyB0D3dQacsd$/bC.gnd6M.WThGD7dlHrZMSfV3fETYAywhYRqMc09qLSArqKS.EpvVtKaxyI15GSp9AWzuBSNRoLx7vsp5jiq/
    github: fquffio
    group: sysadmins
    groups:
      - othergroup
      - sudo
```
